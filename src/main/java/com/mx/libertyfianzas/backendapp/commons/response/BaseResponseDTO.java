package com.mx.libertyfianzas.backendapp.commons.response;

import java.io.Serializable;
/**
 * 
 * @author Gabriel Garcia Gutierrez
 * Clase base para response del web services estatus y mensaje
 */
public class BaseResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int code;

	private String message;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
