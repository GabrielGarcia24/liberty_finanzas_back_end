package com.mx.libertyfianzas.backendapp.commons.response;

/**
 * 
 * @author Gabriel Garcia Gutierrez 
 * Objeto DTO para Fianzas
 */
public class FinanzasDTO extends BaseDTO{

	private static final long serialVersionUID = 1L;
	
	private String fianza;
	private String movimiento;
	private String fiado;
	private int antiguedad;
	private int importe;
	private String color;
	private String moneda;
	private int diasVencimiento;
	private String estado;
	private float precioDolar;
	private float precioEuro;
	
	public float getPrecioDolar() {
		return precioDolar;
	}
	public void setPrecioDolar(float precioDolar) {
		this.precioDolar = precioDolar;
	}
	public float getPrecioEuro() {
		return precioEuro;
	}
	public void setPrecioEuro(float precioEuro) {
		this.precioEuro = precioEuro;
	}
	public int getDiasVencimiento() {
		return diasVencimiento;
	}
	public void setDiasVencimiento(int diasVencimiento) {
		this.diasVencimiento = diasVencimiento;
	}
	public String getFianza() {
		return fianza;
	}
	public void setFianza(String fianza) {
		this.fianza = fianza;
	}
	public String getMovimiento() {
		return movimiento;
	}
	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}
	public String getFiado() {
		return fiado;
	}
	public void setFiado(String fiado) {
		this.fiado = fiado;
	}
	public int getAntiguedad() {
		return antiguedad;
	}
	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}
	public int getImporte() {
		return importe;
	}
	public void setImporte(int importe) {
		this.importe = importe;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	

}
