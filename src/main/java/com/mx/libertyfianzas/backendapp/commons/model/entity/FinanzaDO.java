package com.mx.libertyfianzas.backendapp.commons.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "PRIMAS")
@Entity
public class FinanzaDO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idPrima;

	@Column(name = "fianza")
	private String fianza;
	
	@Column(name = "movimiento")
	private String movimiento;
	
	@Column(name = "fiado")
	private String fiado;
	
	@Column(name = "color")
	private String color;
	
	@Column(name = "moneda")
	private String moneda;
	
	@Column(name = "antiguedad")
	private Integer antiguedad;
	
	@Column(name = "dias_vencimiento")
	private Integer diasVencimiento;
	
	@Column(name = "importe")
	private float importe;
	
	@Column(name = "estado")
	private String estado;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getIdPrima() {
		return idPrima;
	}

	public void setIdPrima(Integer idPrima) {
		this.idPrima = idPrima;
	}

	public String getFianza() {
		return fianza;
	}

	public void setFianza(String fianza) {
		this.fianza = fianza;
	}

	public String getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}

	public String getFiado() {
		return fiado;
	}

	public void setFiado(String fiado) {
		this.fiado = fiado;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Integer getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(Integer antiguedad) {
		this.antiguedad = antiguedad;
	}

	public Integer getDiasVencimiento() {
		return diasVencimiento;
	}

	public void setDiasVencimiento(Integer diasVencimiento) {
		this.diasVencimiento = diasVencimiento;
	}

	public float getImporte() {
		return importe;
	}

	public void setImporte(float importe) {
		this.importe = importe;
	}
	
	
}
