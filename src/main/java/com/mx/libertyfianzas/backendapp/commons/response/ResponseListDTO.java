package com.mx.libertyfianzas.backendapp.commons.response;

import java.util.List;
public class ResponseListDTO extends BaseResponseDTO {

	private static final long serialVersionUID = 1L;
	
	public ResponseListDTO() {}
	
	public ResponseListDTO(int code, List<? extends BaseDTO> data, String message) {
		this.data = data;
		this.setCode(code);
		this.setMessage(message);
	}
	
	private List<? extends BaseDTO> data;
	
	public List<? extends BaseDTO> getData() {
		return data;
	}

	public void setData(List<? extends BaseDTO> data) {
		this.data = data;
	}
}
