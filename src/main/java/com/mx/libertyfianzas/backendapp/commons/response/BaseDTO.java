package com.mx.libertyfianzas.backendapp.commons.response;

import java.io.Serializable;

/**
 * 
 * @author Gabriel Garcia
 * Clase base para objetos DTO
 */

public class BaseDTO implements Serializable {

	/**
	 * A unique serial version identifier
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

}
