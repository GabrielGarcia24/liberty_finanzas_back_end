package com.mx.libertyfianzas.backendapp.service;

import com.mx.libertyfianzas.backendapp.commons.response.ResponseListDTO;

public interface BaseService {
	
	ResponseListDTO getAll();
	
	ResponseListDTO findByEstado (String estado);

}
