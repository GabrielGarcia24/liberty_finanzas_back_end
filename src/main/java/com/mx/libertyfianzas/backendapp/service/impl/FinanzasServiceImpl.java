package com.mx.libertyfianzas.backendapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.libertyfianzas.backendapp.commons.model.entity.FinanzaDO;
import com.mx.libertyfianzas.backendapp.commons.response.FinanzasDTO;
import com.mx.libertyfianzas.backendapp.commons.response.ResponseListDTO;
import com.mx.libertyfianzas.backendapp.repository.FianzaRepository;
import com.mx.libertyfianzas.backendapp.service.BaseService;

@Service
public class FinanzasServiceImpl implements BaseService{
	
	@Autowired
	FianzaRepository fianzaRepository;

	@Override
	public ResponseListDTO findByEstado(String estado) {
		List<FinanzasDTO> listDTO = new ArrayList<FinanzasDTO>();
		ResponseListDTO responseListDTO = new ResponseListDTO();
		Iterable<FinanzaDO> allPrimas = fianzaRepository.findByEstado(estado);
		
		
		for (FinanzaDO finanzaDO : allPrimas) {
			FinanzasDTO finanzasDTO = new FinanzasDTO();
			finanzasDTO.setId(finanzaDO.getIdPrima());
			finanzasDTO.setAntiguedad(finanzaDO.getAntiguedad());
			finanzasDTO.setFiado(finanzaDO.getFiado());
			finanzasDTO.setFianza(finanzaDO.getFianza());
			finanzasDTO.setImporte((int) finanzaDO.getImporte());
			finanzasDTO.setMovimiento(finanzaDO.getMovimiento());
			finanzasDTO.setMoneda(finanzaDO.getMovimiento());
			finanzasDTO.setDiasVencimiento(finanzaDO.getDiasVencimiento());
			finanzasDTO.setPrecioDolar((float) (finanzaDO.getImporte() / 20.5));
			finanzasDTO.setPrecioEuro((float) (finanzaDO.getImporte() / 20.3));
			
			finanzasDTO.setColor(finanzaDO.getDiasVencimiento() > 0 ? "red" : "blue");
			listDTO.add(finanzasDTO);
			
		}
		
		responseListDTO.setData(listDTO);		
		return responseListDTO;
	}

	@Override
	public ResponseListDTO getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	
	

}
