package com.mx.libertyfianzas.backendapp.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.libertyfianzas.backendapp.commons.constants.ErrorConstans;
import com.mx.libertyfianzas.backendapp.commons.constants.SuccessConstants;
import com.mx.libertyfianzas.backendapp.commons.response.BaseResponseDTO;
import com.mx.libertyfianzas.backendapp.commons.response.ResponseListDTO;
import com.mx.libertyfianzas.backendapp.service.impl.FinanzasServiceImpl;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/finanzas")
public class WebServiceFianzasController {
	
	private static final Logger logger = Logger.getLogger(WebServiceFianzasController.class);
	
	@Autowired
	private FinanzasServiceImpl finanzasService;
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	 * Expone servicio de consulta de todas las primas en BD
	 * @return responseEntity 
	 */
	@ResponseBody
	@GetMapping(value= "/{type}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDTO> getAll(@PathVariable("type") String type) {
		ResponseListDTO listDTO = null;

		try {
			
			logger.info("Controller: WebServiceFinanzas Metodo getAll()");
			
			String message = this.messageSource.getMessage(SuccessConstants.PREFIX + SuccessConstants.GENERIC, null, LocaleContextHolder.getLocale());
			listDTO = this.finanzasService.findByEstado(type);
			listDTO.setCode(200);
			listDTO.setMessage(message);
		} catch (Exception e) {
			logger.error("", e);
			String message = this.messageSource.getMessage(ErrorConstans.PREFIX + ErrorConstans.GENERIC, null, LocaleContextHolder.getLocale());
			listDTO = new ResponseListDTO(); 
			listDTO.setCode(500);
			listDTO.setMessage(message);
		}

		return new ResponseEntity<BaseResponseDTO>(listDTO, HttpStatus.OK);
	}
}
