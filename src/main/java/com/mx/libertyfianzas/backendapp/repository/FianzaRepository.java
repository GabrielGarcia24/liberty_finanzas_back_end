package com.mx.libertyfianzas.backendapp.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import com.mx.libertyfianzas.backendapp.commons.model.entity.FinanzaDO;


public interface FianzaRepository extends CrudRepository<FinanzaDO, Integer>, JpaSpecificationExecutor<FinanzaDO>{

	Iterable<FinanzaDO> findByEstado(String estado);
}
